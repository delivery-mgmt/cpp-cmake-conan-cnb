This is a POC Cloud Native Buildpack for building and running cpp applications that use CMake and Conan.  I doubt this will work in the wild, but serves as starting point for CNB creation. (and had worked with a couple sample applications).


```bash

pack build <image name> --path <path to project using cmake and conan> --buildpack <this project> --env=executable_name=<name of executable to run>

```
